package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
	"testing"

	"github.com/pelletier/go-toml"
	"github.com/stretchr/testify/assert"
)

var revocations = map[string]string{
	"aws":                          "gitleaks_rule_id_aws",
	"gitlab_personal_access_token": "gitleaks_rule_id_gitlab_personal_access_token",
	"pypi_upload_token":            "gitleaks_rule_id_pypi_upload_token",
	"gcp_api_key":                  "gitleaks_rule_id_gcp_api_key",
	"google_(gcp)_service-account": "gitleaks_rule_id_google_(gcp)_service-account",
	"gcp_oauth_client_secret":      "gitleaks_rule_id_gcp_oauth_client_secret",
}

type GitleaksRulesConfig struct {
	Title     string
	AllowList AllowList
	Rules     []Rule
}

type AllowList struct {
	Description string
	Regexes     []string
	Commits     []string
	Files       []string
	Paths       []string
	Repos       []string
}

type Rule struct {
	ID          string
	Description string
	Regex       string
	File        string
	Path        string
	ReportGroup int
	Tags        []string
	Entropies   []struct {
		Min   string
		Max   string
		Group string
	}
	AllowList AllowList
}

// TestRevocationTypes ensures we are generating rule IDs for revocable
// token types that match the same format computed by the
// `ScanSecurityReportSecretsWorker` in Rails. These computed values are
// also used in the internal secret-revocation-service. Context:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/398e5a40493ace6d86420f40f42ecf9507371e22/ee/app/workers/scan_security_report_secrets_worker.rb#L50-53
// - https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/secret-revocation-service/-/blob/cbfe43286c2863d055979ba51e7e7ea43f1047df/srs/__init__.py#L19-22
func TestRevocationTypes(t *testing.T) {
	revocationRules := revocationRuleTypes()
	assert.Equal(t, len(revocations), len(revocationRules), fmt.Sprintf("revocation_type rules should equal %d", len(revocations)))
	for _, rule := range revocationRules {
		secret := Secret{
			Rule:        rule.ID,
			Description: rule.ID,
		}
		identifier := fmt.Sprintf("%s_%s", strings.ToLower(string(secret.identifiers()[0].Type)), slugify(secret.identifiers()[0].Value))
		assert.Equal(t, revocations[slugify(rule.ID)], identifier, "should be equal")
	}
}

// TestGitleaksConfig validates the gitleaks.toml configuration file. Code borrowed from
// https://github.com/zricethezav/gitleaks/blob/e6934bf0a5b87024c40417bfc5e59cfa5539b63c/config/config.go#L95-L234.
func TestGitleaksConfig(t *testing.T) {
	tomlLoader := rulesConfig()
	for _, rule := range tomlLoader.Rules {
		// check and make sure the rule is valid
		if rule.Regex == "" && rule.Path == "" && rule.File == "" && len(rule.Entropies) == 0 {
			t.Fatalf("invalid gitleaks toml, %v is not an actionable rule ", rule)
		}
		re, err := regexp.Compile(rule.Regex)
		if err != nil {
			t.Fatalf("invalid gitleaks toml %v", err)
		}
		_, err = regexp.Compile(rule.File)
		if err != nil {
			t.Fatalf("invalid gitleaks toml %v", err)
		}
		_, err = regexp.Compile(rule.Path)
		if err != nil {
			t.Fatalf("invalid gitleaks toml %v", err)
		}

		// rule specific regexes
		for _, re := range rule.AllowList.Regexes {
			_, err := regexp.Compile(re)
			if err != nil {
				t.Fatalf("invalid gitleaks toml %v", err)
			}
		}

		// rule specific filenames
		for _, re := range rule.AllowList.Files {
			_, err := regexp.Compile(re)
			if err != nil {
				t.Fatalf("invalid gitleaks toml %v", err)
			}
		}

		// rule specific paths
		for _, re := range rule.AllowList.Paths {
			_, err := regexp.Compile(re)
			if err != nil {
				t.Fatalf("invalid gitleaks toml %v", err)
			}
		}
		for _, e := range rule.Entropies {
			min, err := strconv.ParseFloat(e.Min, 64)
			if err != nil {
				t.Fatalf("invalid gitleaks toml %v", err)
			}
			max, err := strconv.ParseFloat(e.Max, 64)
			if err != nil {
				t.Fatalf("invalid gitleaks toml %v", err)
			}
			if e.Group == "" {
				e.Group = "0"
			}
			group, err := strconv.ParseInt(e.Group, 10, 64)
			if err != nil {
				t.Fatalf("invalid gitleaks toml %v", err)
			} else if int(group) >= len(re.SubexpNames()) {
				t.Fatalf("problem loading config: group cannot be higher than number of groups in regexp. rule %v", rule)
			} else if group < 0 {
				t.Fatalf("problem loading config: group cannot be lower than 0. rule %v", rule)
			} else if min > 8.0 || min < 0.0 || max > 8.0 || max < 0.0 {
				t.Fatalf("problem loading config: invalid entropy ranges, must be within 0.0-8.0 %v", rule)
			} else if min > max {
				t.Fatalf("problem loading config: entropy Min value cannot be higher than Max value %v", rule)
			}
		}
	}

	// global regex allowLists
	for _, allowListRegex := range tomlLoader.AllowList.Regexes {
		_, err := regexp.Compile(allowListRegex)
		if err != nil {
			t.Fatalf("invalid gitleaks toml %v", err)
		}
	}

	// global file name allowLists
	for _, allowListFileName := range tomlLoader.AllowList.Files {
		_, err := regexp.Compile(allowListFileName)
		if err != nil {
			t.Fatalf("invalid gitleaks toml %v", err)
		}
	}

	// global file path allowLists
	for _, allowListFilePath := range tomlLoader.AllowList.Paths {
		_, err := regexp.Compile(allowListFilePath)
		if err != nil {
			t.Fatalf("invalid gitleaks toml %v", err)
		}
	}

	// global repo allowLists
	for _, allowListRepo := range tomlLoader.AllowList.Repos {
		_, err := regexp.Compile(allowListRepo)
		if err != nil {
			t.Fatalf("invalid gitleaks toml %v", err)
		}
	}
}

func revocationRuleTypes() []Rule {
	revocationRules := []Rule{}
	for _, rule := range rulesConfig().Rules {
		for _, tag := range rule.Tags {
			if tag == "revocation_type" {
				revocationRules = append(revocationRules, rule)
			}
		}
	}
	fmt.Println(revocationRules)
	return revocationRules
}

func rulesConfig() GitleaksRulesConfig {
	b, err := ioutil.ReadFile("./gitleaks.toml")
	if err != nil {
		panic(err)
	}

	rulesConfig := GitleaksRulesConfig{}
	if err = toml.Unmarshal(b, &rulesConfig); err != nil {
		panic(err)
	}

	return rulesConfig
}

func slugify(str string) string {
	return strings.ReplaceAll(strings.ToLower(str), " ", "_")
}
